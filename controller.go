package main

import (
	"fmt"

	"gitlab.com/justas2481/network"
	"gitlab.com/justas2481/network/registry"
)

const (
	firstAction uint = iota
	secondAction
)

func registerActions() registry.Commands {
	commands := registry.Commands{}

	commands.Register(firstAction, func(msg *network.Message, errC chan<- error) {
		fmt.Println("First action invoked.")

		// Send something back to peer.
		client, err := pool.Client(msg.ClientId())
		if err != nil {
			errC <- err
		}

		data := struct {
			Name string `json:"name"`
		}{"Server"}
		message := network.Message{Cmd: 0, Payload: data}
		message.SetClientId(msg.ClientId())
		client.Send <- message

		errC <- nil
	})

	commands.Register(secondAction, func(msg *network.Message, errC chan<- error) {
		fmt.Println("Second action invoked.")

		errC <- nil
	})

	return commands
}
