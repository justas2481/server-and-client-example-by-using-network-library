package main

import (
	"log"

	"gitlab.com/justas2481/network"
	"gitlab.com/justas2481/network/registry"
)

type app struct {
	tasksCounter registry.Tasks
	messagesC    chan *network.Message
	errC         chan error
}

// GetMessageChan returns a channel
// on which pool can comunicate with main application.
func (a app) GetMessageChan() chan<- *network.Message {
	return a.messagesC
}

// GetErrorChan returns a channel
// which is used to send errors to the pool.
func (a app) GetErrorChan() <-chan error {
	return a.errC
}

// processMessage invokes a handler attached to the command.
func (a app) ProcessMessage(msg *network.Message) error {
	execute, err := commands.Retrieve(msg.Cmd)
	if err != nil {
		return err
	}

	go execute(msg, a.errC)

	return nil
}

// Run runs listener for the tasks from a pool and deals with their resolution.
func (a app) Run() {
	// Listen for incomming messages.
	for msg := range a.messagesC {
		// TODO: Do only tasks wich are only first for a specific client.

		if err := a.ProcessMessage(msg); err != nil {
			log.Println("Unable to process message:", err)
			return
		}

	}
}
