# Server and client example by using Network library

This is an example of how to use my [Web Socket Network library](https://gitlab.com/justas2481/network).

## How to use

Just clone this project and run it with
```
go run .
```
Server will automatically serve a ws.html file on "/" route. All the server configurations can be changed in network library if needed.