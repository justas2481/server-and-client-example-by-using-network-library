// Simple server.
package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/justas2481/network"
	"gitlab.com/justas2481/network/registry"

	"github.com/google/uuid"
)

var (
	application = app{
		tasksCounter: registry.Tasks{},
		messagesC:    make(chan *network.Message),
		errC:         make(chan error),
	}
	pool     = network.NewPool(application)
	commands = registerActions()
)

func main() {
	setupRoutes()
	go pool.Start()
	log.Println("Starting server...")
	log.Fatal(http.ListenAndServe(network.DefaultAddress+":"+network.DefaultPort, nil))
}

func setupRoutes() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "ws.html")
	})
	http.HandleFunc("/ws", initializeWebsocket)
}

func initializeWebsocket(w http.ResponseWriter, r *http.Request) {
	// Obtain an ID.
	id, err := uuid.NewRandom()
	if err != nil {
		fmt.Println("Can not create UUID.")
		return
	}

	if err := pool.StartNewConnection(id, w, r); err != nil {
		log.Println(err)
		return
	}
}
