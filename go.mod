module gitlab.com/justas2481/server-and-client-example-by-using-network-library

go 1.18

require (
	github.com/google/uuid v1.3.0
	gitlab.com/justas2481/network v0.1.0
)

require github.com/gorilla/websocket v1.5.0 // indirect
